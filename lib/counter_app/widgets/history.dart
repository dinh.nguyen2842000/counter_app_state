import 'package:flutter/material.dart';

class HistoryWidget extends StatefulWidget {
  final List<int> increaseHistory;

  const HistoryWidget({
    Key? key,
    required this.increaseHistory,
  }) : super(key: key);

  @override
  State<HistoryWidget> createState() => _HistoryWidgetState();
}

class _HistoryWidgetState extends State<HistoryWidget> {
  late ListView list = buildList();

  final scrollController = ScrollController();

  ListView buildList() {
    return ListView.separated(
      scrollDirection: Axis.horizontal,
      itemBuilder: (_, index) {
        return Card(
          child: SizedBox(
            height: 30,
            width: 30,
            child: Center(
              child: Text('${widget.increaseHistory[index]}'),
            ),
          ),
        );
      },
      separatorBuilder: (_, __) {
        return const SizedBox(
          width: 10,
        );
      },
      itemCount: widget.increaseHistory.length,
    );
  }

  @override
  void didUpdateWidget(covariant HistoryWidget oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (widget.increaseHistory.length != oldWidget.increaseHistory.length) {
      list = buildList();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        const Text('Increase counter'),
        Flexible(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 15),
            child: SizedBox(
              height: 40,
              child: list,
            ),
          ),
        ),
      ],
    );
  }
}
