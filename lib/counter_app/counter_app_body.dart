import 'package:flutter/material.dart';
import 'package:flutter_counter/counter_app/widgets/app_title.dart';
import 'package:flutter_counter/counter_app/widgets/history.dart';

class CounterAppBody extends StatefulWidget {
  const CounterAppBody({
    Key? key,
  }) : super(key: key);

  @override
  State<CounterAppBody> createState() => _CounterAppBodyState();
}

class _CounterAppBodyState extends State<CounterAppBody> {
  int counter = 0;

  List<int> increaseHistory = [];

  void increase() {
    setState(() {
      counter++;
      increaseHistory = List<int>.from(increaseHistory)..add(counter);
    });
  }

  void decrease() {
    setState(() {
      counter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            // AppTitle
            const AppTitle(),
            //
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                ElevatedButton(
                  onPressed: decrease,
                  child: const Text('-'),
                ),
                Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: Text(
                    '$counter',
                    style: const TextStyle(
                      color: Colors.blue,
                      fontSize: 22,
                    ),
                  ),
                ),
                ElevatedButton(
                  onPressed: increase,
                  child: const Text('+'),
                ),
              ],
            ),
            //
            HistoryWidget(increaseHistory: increaseHistory),
          ],
        ),
      ),
    );
  }
}
