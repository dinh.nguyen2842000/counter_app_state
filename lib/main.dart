import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_counter/counter_app/counter_app_body.dart';

void main() {
  runApp(
    const EnhancedCounterApp(),
  );
}

class EnhancedCounterApp extends StatelessWidget {
  const EnhancedCounterApp({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: CounterAppBody(),
      debugShowCheckedModeBanner: false,
    );
  }
}
